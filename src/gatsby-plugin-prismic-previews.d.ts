declare module "gatsby-plugin-prismic-previews" {
  import { ComponentType, ReactNode } from "react"
  import { PageProps } from "gatsby"

  export function withPrismicPreview<P>(
    component: ComponentType<PageProps<P>>,
    options: any
  ): ComponentType<PageProps<P>>

  export const PrismicPreviewProvider: ComponentType<{ children: ReactNode }>

  export function withPrismicPreviewResolver<P>(
    component: ComponentType<PageProps<P>>
  ): ComponentType<PageProps<P>>
}
