import { graphql, navigate, PageProps } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import React, { useEffect, useState } from "react"

import {
  SafetyAlertDocument,
  SafetyAlertsPageDocument,
} from "../../types.generated"
import Layout from "../components/Layout"
import SafetyAlertCarousel from "../components/SafetyAlertsCarousel"
import { SEO } from "../components/SEO"
import ULLogo from "../images/svg/ul_logo_white.svg"

const SafetyAlertHeader: React.FC = () => (
  <div className="flex bg-[#2355a0] text-white items-stretch relative max-w-2xl overflow-hidden min-h-[72px]">
    <div className="flex-1 flex w-2/3">
      <div className="pl-3 py-2 flex-col flex content-between justify-between">
        <div className="w-28">
          <ULLogo />
        </div>
        <div className="text-xl font-semibold mt-1">Safety Alerts</div>
      </div>
    </div>

    <div className="absolute bg-[#0c2756] w-52 h-52 sm:w-72 sm:h-72 rounded-full -right-10 -top-3/4 sm:-top-24"></div>
    <div className="z-10 flex pr-2 items-center justify-end w-36 sm:w-72">
      <div className="pl-0 mr-2">
        <p className="text-[8px] flex-wrap">
          Stay informed and protected.
          <br /> Learn more about digital safety.
        </p>
      </div>
      <div className="flex items-center justify-center">
        <StaticImage
          src="../images/ul_icon_security_lock.png"
          alt="timeline"
          height={60}
        />
      </div>
    </div>
  </div>
)

const SafetyAlertFooter: React.FC<SafetyAlertDocument> = ({ slide }) => {
  const dateString = slide.first_publication_date
    ? new Date(slide.first_publication_date).toLocaleDateString("en-US", {
        year: "numeric",
        month: "short",
        day: "numeric",
      })
    : null

  return (
    <div className="grid grid-rows-2 grid-cols-3 bg-[#2355a0] text-white max-w-2xl gap-y-4 gap-x-1 text-[10px] min-h-[60px] sm:px-4 px-2 py-3 relative z-10 overflow-hidden">
      {/* Decorative Element */}
      <div className="absolute bg-[#0c2756] w-44 h-44 rounded-full -right-6 -top-16"></div>

      {/* Row 1 */}
      <div className="flex items-center gap-1">
        <div className="flex flex-nowrap">{slide.uid}</div>{" "}
        {dateString && (
          <>
            |<div>{dateString}</div>
          </>
        )}
      </div>

      <div className="flex items-center flex-wrap z-10">
        {slide.data.tags && slide.data.tags.length > 0 ? (
          <>
            {slide.data.tags.map((tag, index) => (
              <span key={index} className="mr-2">
                {tag.tag}
              </span>
            ))}
          </>
        ) : (
          <div className="opacity-0">&nbsp;</div>
        )}
      </div>

      <div className="flex justify-end items-center z-10">
        {slide.data.author_name && slide.data.author_link ? (
          <p className="text-end">
            Reported by
            <br />
            <a className="underline" href={slide.data.author_link?.url}>
              {slide.data.author_name}
            </a>
          </p>
        ) : (
          <div className="opacity-0 text-[8px]">Placeholder</div>
        )}
      </div>

      {/* Row 2 */}
      <div className="font-semibold">
        <a href="dsri.org">dsri.org</a>
      </div>

      <div className="font-semibold">
        1603 Orrington Ave, Evanston, IL 60201
      </div>

      <div className="flex justify-end z-10">
        <a href="mailto:dsri@ul.org">dsri@ul.org</a>
      </div>
    </div>
  )
}

const SafetyAlertPage: React.FC<SafetyAlertsPageDocument> = ({
  data,
  location,
  pageContext,
}) => {
  const { uid: currentUid } = pageContext

  const alertsExist = data.prismicSafetyAlertsPage?.data?.alerts.length > 0

  useEffect(() => {
    if (!alertsExist) {
      navigate("/404", { replace: true }) // Redirect to 404 if no alert data
    }
  }, [alertsExist])

  if (!alertsExist) {
    return null
  }

  let alerts = data.prismicSafetyAlertsPage.data.alerts

  alerts = alerts.filter((alert: SafetyAlertDocument) => alert.alert.document)

  const slides = alerts.map((alert: SafetyAlertDocument) => {
    return {
      ...alert.alert.document,
    }
  })

  const initialSlide = slides.find(
    (slide: SafetyAlertDocument) => slide.uid === currentUid
  )

  const [currentSlide, setCurrentSlide] = useState(initialSlide)

  const handleSlideChange = (newSlideIndex: number) => {
    setCurrentSlide(slides[newSlideIndex])
  }

  return (
    <Layout>
      <main className="container mx-auto max-w-xl min-h-[calc(100vh-64px)] flex flex-col md:py-10">
        <SafetyAlertHeader />
        <div className="flex-grow">
          <SafetyAlertCarousel
            slides={slides}
            currentSlideUid={currentUid}
            location={location}
            onSlideChange={handleSlideChange}
          />
        </div>
        {/* Subscribe link */}
        <div className="flex items-center justify-center py-4">
          <a
            href="http://go.pardot.com/l/1068562/2024-09-11/pr59b7"
            className="text-[#2355a0] underline text-center"
          >
            Subscribe to Safety Alerts
          </a>
        </div>
        <SafetyAlertFooter slide={currentSlide} />
      </main>
    </Layout>
  )
}

export const query = graphql`
  query AllSafetyAlerts {
    prismicSafetyAlertsPage {
      data {
        alerts {
          alert {
            document {
              ... on PrismicSafetyAlert {
                uid
                first_publication_date
                data {
                  author_name
                  author_link {
                    url
                  }
                  tags {
                    tag
                  }
                  media {
                    gatsbyImageData
                    alt
                  }
                  text {
                    richText
                    text
                  }
                  steps_title
                  steps {
                    step {
                      richText
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`

export const Head = (props: PageProps) => {
  const slides = props?.data?.prismicSafetyAlertsPage?.data?.alerts || []
  const currentSlide = slides.find(
    (slide: SafetyAlertDocument) =>
      slide.alert.document.uid === props.pageContext.uid
  )
  if (currentSlide) {
    const metaImage =
      currentSlide.alert.document?.data?.media?.gatsbyImageData?.images
        ?.fallback?.src
    return (
      <SEO
        title={`Safety Alert ${currentSlide.alert.document.uid}`}
        description={currentSlide.alert.document.data.text.text}
        pathname={props.location.pathname}
        metaImage={metaImage}
      />
    )
  } else {
    return (
      <SEO
        title="Safety Alerts"
        description="Stay informed and protected. Learn more about digital safety."
        pathname={props.location.pathname}
      />
    )
  }
}

export default SafetyAlertPage
