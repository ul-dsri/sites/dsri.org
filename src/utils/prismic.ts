export const dateToUTC = (date: string | null) => {
  let parsedDate = null
  if (date) {
    // to convert to UTC-0 and display the correct date
    const pageDate = date

    // Split the date string into year, month and day
    const [yearStr, monthStr, dayStr] = pageDate.split("-")

    // Convert the day, month, and year to numbers
    const month = parseInt(monthStr, 10)
    const day = parseInt(dayStr, 10)
    const year = parseInt(yearStr, 10)

    parsedDate = new Date(year, month - 1, day, 0, 0, 0, 0)
    return parsedDate
  }
}
