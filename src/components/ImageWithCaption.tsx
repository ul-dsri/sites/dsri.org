import React from "react"

interface ImageWithCaptionProps {
  node: {
    url: string
    alt: string
  }
}

const ImageWithCaption: React.FC<ImageWithCaptionProps> = ({ node }) => {
  return (
    <figure style={{ display: "block", textAlign: "center", margin: 0 }}>
      <img
        src={node.url}
        alt={node.alt}
        style={{ display: "block", maxWidth: "100%" }}
      />
      {node.alt && (
        <figcaption
          style={{ fontSize: "0.875rem", color: "#555", marginTop: "0.5rem" }}
        >
          {node.alt}
        </figcaption>
      )}
    </figure>
  )
}

export default ImageWithCaption
