import { GatsbyImage, IGatsbyImageData } from "gatsby-plugin-image"
import React from "react"

type HeroBannerProps = {
  text: string | null | undefined
  subtitle: string | null | undefined
  bannerImage:
    | {
        gatsbyImageData: IGatsbyImageData
        alt: string | null | undefined
      }
    | undefined
}

const HeroBanner = ({ text, subtitle, bannerImage }: HeroBannerProps) => (
  <div className="w-screen mx-auto grid">
    <GatsbyImage
      style={{
        gridArea: "1/1",
        maxHeight: "700px",
      }}
      image={bannerImage?.gatsbyImageData}
      alt={bannerImage?.alt || ""}
      formats={["auto", "webp", "avif"]}
      className="w-full aspect-[5/2]"
    />
    <div
      className="relative grid place-items-center"
      style={{
        // By using the same grid area for both, they are stacked on top of each other.
        gridArea: "1/1",
      }}
    >
      <div className="container bg-black bg-opacity-50 text-white w-5/6 md:w-1/2 top-1/2 p-2">
        {text && <h1 className="text-2xl md:text-5xl px-4 py-2">{text}</h1>}
        {subtitle && (
          <h2 className="text-md md:text-2xl px-4 py-2">{subtitle}</h2>
        )}
      </div>
    </div>
  </div>
)

export default HeroBanner
