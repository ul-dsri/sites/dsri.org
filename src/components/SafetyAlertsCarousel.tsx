import { PrismicRichText } from "@prismicio/react"
import { GatsbyImage, IGatsbyImageData } from "gatsby-plugin-image"
import React, { useEffect, useState } from "react"
import Slider from "react-slick"

import "slick-carousel/slick/slick.css"

import { SafetyAlertDocument } from "../../types.generated"

interface SlideProps {
  uid: string
  media: any
  text: any
  steps: {
    step: { richText: any }
  }[]
  steps_title: string
}

interface CarouselProps {
  slides: SlideProps[]
  currentSlideUid: string
  onSlideChange: (index: number) => void
}

const StepNumber = ({ number }: { number: number }) => {
  return (
    <div className="flex flex-shrink-0 items-center justify-center w-6 h-6 rounded-full bg-[#2355a0] text-white text-xs font-semibold">
      {number}
    </div>
  )
}

function NextArrow(props: any) {
  const { className, style, onClick, isMobile } = props

  return (
    <div
      className={className}
      style={{
        ...style,
        position: "absolute",
        top: isMobile ? "97%" : "100%",
        right: isMobile ? "10px" : "0",
        bottom: isMobile ? "50%" : "auto",
        width: isMobile ? "50px" : "50px",
        height: isMobile ? "50px" : "50px",
        borderRadius: isMobile ? "" : "50%",
        display: "flex",
        alignItems: "center",
        justifyContent: isMobile ? "flex-start" : "center",
        paddingLeft: isMobile ? "10px" : "0",
        cursor: "pointer",
        transform: isMobile ? "translateY(50%)" : "none",
        zIndex: 1,
      }}
      onClick={onClick}
    >
      <span
        style={{
          color: "#2355a0",
          fontSize: "40px",
        }}
      >
        →
      </span>
    </div>
  )
}

function PrevArrow(props: any) {
  const { className, style, onClick, isMobile } = props

  return (
    <div
      className={className}
      style={{
        ...style,
        position: "absolute",
        top: isMobile ? "97%" : "100%",
        left: isMobile ? "10px" : "0",
        width: isMobile ? "50px" : "50px",
        height: isMobile ? "50px" : "50px",
        borderRadius: isMobile ? "0 70px 70px 0" : "50%",
        display: "flex",
        alignItems: "center",
        justifyContent: isMobile ? "flex-end" : "center",
        paddingRight: isMobile ? "10px" : "0",
        cursor: "pointer",
        transform: isMobile ? "translateY(50%)" : "none",
        zIndex: 1,
      }}
      onClick={onClick}
    >
      <span
        style={{
          color: "#2355a0",
          fontSize: "40px",
        }}
      >
        ←
      </span>
    </div>
  )
}

const SafetyAlertsCarousel: React.FC<CarouselProps> = ({
  slides,
  currentSlideUid,
  onSlideChange,
}) => {
  const currentSlideIndex = slides.findIndex(
    (slide: { uid: any }) => slide.uid === currentSlideUid
  )

  const isBrowser = () => typeof window !== "undefined"

  const [isMobile, setIsMobile] = useState(false)

  let scrollPosition = 0

  const handleBeforeChange = (_current: number, next: number) => {
    if (isBrowser()) {
      scrollPosition = window.scrollY
      const nextSlideUid = slides[next].uid
      onSlideChange(next)

      window.history.replaceState(null, "", `/safety-alerts/${nextSlideUid}`)

      window.scrollTo(0, scrollPosition)
    }
  }

  useEffect(() => {
    if (isBrowser()) {
      const handleResize = () => {
        setIsMobile(window.innerWidth < 768)
      }

      handleResize() // Initial check on mount
      window.addEventListener("resize", handleResize)

      return () => {
        window.removeEventListener("resize", handleResize)
      }
    }
  }, [])

  const [swipeEnabled, setSwipeEnabled] = useState(true)

  const settings = {
    initialSlide: currentSlideIndex,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    beforeChange: handleBeforeChange, // Capture scroll before slide changes and restore after
    nextArrow: slides.length > 1 ? <NextArrow isMobile={isMobile} /> : <></>,
    prevArrow: slides.length > 1 ? <PrevArrow isMobile={isMobile} /> : <></>,
    speed: 400, // Optional: Duration of the slide transition in milliseconds
    cssEase: "ease-in-out", // Easing function for smooth transitions
    adaptiveHeight: true, // Adjust slide height based on content
    swipe: swipeEnabled, // Enable swipe gestures
  }

  return (
    <div className="h-full flex-grow flex">
      <Slider {...settings} className="h-full w-full">
        {slides.map((slide: SafetyAlertDocument, index: any) => (
          <div key={slide.uid} className="h-full flex flex-col">
            <header className="mb-2">
              {slide.data.media && (
                <div>
                  <GatsbyImage
                    className="w-full max-h-96 object-cover aspect-[4/3]"
                    image={slide.data.media.gatsbyImageData}
                    alt={slide.data.media.alt}
                  />
                </div>
              )}
            </header>
            <div
              className={`px-4 flex-grow flex flex-col justify-between select-text`}
              onMouseEnter={() => setSwipeEnabled(false)}
              onMouseLeave={() => setSwipeEnabled(true)}
            >
              <div className="text-sm lg:text-base prose">
                <PrismicRichText field={slide.data.text.richText} />
              </div>
              <div className="mt-4">
                <h2 className="font-semibold">{slide.data.steps_title}</h2>

                <div className="space-y-4 mt-2 mb-2">
                  {slide.data.steps.map((step: any, stepIndex: number) => (
                    <div
                      key={`step-${stepIndex}`}
                      className="flex items-start space-x-3"
                    >
                      <StepNumber number={stepIndex + 1} />
                      <div className="text-sm prose">
                        <PrismicRichText field={step.step.richText} />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  )
}

export default SafetyAlertsCarousel
