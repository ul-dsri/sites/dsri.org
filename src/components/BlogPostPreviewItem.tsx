import { faArrowUpRightFromSquare } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Link } from "gatsby"
import { GatsbyImage, IGatsbyImageData } from "gatsby-plugin-image"
import React, { ReactElement } from "react"

import stockImage from "../images/stock_banner_2.jpg"

type BlogPostPreviewItemProps = {
  title: string
  date: string
  preview?: string
  destination: string
  isExternal: boolean
  thumbnailImage?: { gatsbyImageData: IGatsbyImageData; alt: string }
  thumbnailAlt?: string
  tags?: string[]
}

const BlogPostPreviewItem = ({
  title,
  date,
  preview,
  destination,
  isExternal,
  thumbnailImage,
  thumbnailAlt,
}: BlogPostPreviewItemProps): ReactElement => {
  const dateObj = new Date(date)
  return (
    <div className="flex flex-col overflow-hidden shadow-lg">
      <div>
        <a href={destination}>
          <div className="w-full aspect-[3/2]">
            {thumbnailImage?.gatsbyImageData ? (
              <GatsbyImage
                image={thumbnailImage.gatsbyImageData}
                alt={thumbnailImage.alt || ""}
                className="w-full aspect-[3/2]"
              />
            ) : (
              // Fallback thumbnail.
              <img
                className="w-full aspect-[3/2]"
                src={stockImage}
                alt={thumbnailAlt}
              />
            )}
          </div>
        </a>
      </div>

      <div className="px-4 mt-3 grow">
        <h2 className="text-2xl hover:text-ul-bright-blue leading-9 transition duration-300">
          <a href={destination}>{title}</a>
        </h2>

        <p className="mt-2 font-light">{dateObj.toDateString()}</p>

        {preview && <p className="mt-5 leading-7 font-light">{preview}</p>}
      </div>

      <div className="px-4 py-4 text-ul-bright-blue">
        {isExternal ? (
          <a href={destination}>
            <p>
              Read More (external){" "}
              <FontAwesomeIcon icon={faArrowUpRightFromSquare} />
            </p>
          </a>
        ) : (
          <Link to={destination}>
            <p>Read More →</p>
          </Link>
        )}
      </div>
    </div>
  )
}

export default BlogPostPreviewItem
