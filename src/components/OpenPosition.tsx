import { RichTextField } from "@prismicio/client"
import { PrismicRichText } from "@prismicio/react"
import React, { ReactElement } from "react"

import ProseBlock from "./ProseBlock"

const Button = ({ text, url }: { text: string; url: string }): ReactElement => (
  <a
    href={url}
    className="bg-ul-bright-blue text-white font-semibold py-2 px-5 rounded-full hover:bg-black transition duration-300 block sm:inline-block text-center"
  >
    {text}
  </a>
)

type OpenPositionProps = {
  title: string
  url: string
  description: RichTextField
}

const OpenPosition = ({
  title,
  url,
  description,
}: OpenPositionProps): ReactElement => (
  <div className="flex flex-wrap flex-col py-6 gap-1 rounded-t-lg">
    <div className="flex flex-row mb-3">
      <a href={url}>
        <h1 className="text-xl font-semibold hover:text-ul-bright-blue transition duration-300">
          {title}
        </h1>
        <span className="font-light">Remote (US)</span>
      </a>
    </div>
    <ProseBlock>
      <PrismicRichText
        field={description}
        components={{
          ListItem: ({ children, key }) => (
            <li className="py-1" key={key}>
              {children}
            </li>
          ),
          List: ({ children, key }) => (
            <ol className="list-decimal pl-6" key={key}>
              {children}
            </ol>
          ),
          oListItem: ({ children, key }) => (
            <li className="py-1" key={key}>
              {children}
            </li>
          ),
          oList: ({ children, key }) => (
            <ol className="list-decimal pl-6" key={key}>
              {children}
            </ol>
          ),
        }}
      />
    </ProseBlock>
    <div>
      <Button text="Apply" url={url} />
    </div>
  </div>
)

export default OpenPosition
