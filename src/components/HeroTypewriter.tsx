import React from "react"
import Typewriter from "typewriter-effect"

type HeroTypewriterProps = {
  typewriterStrings: Array<string>
}

const HeroTypewriter = ({ typewriterStrings }: HeroTypewriterProps) => (
  <div className="container mx-auto">
    <div className="container flex flex-col px-4 max-w-5xl py-16 md:py-28 text-4xl md:text-6xl mx-auto justify-items-start">
      <div className="text-left font-mono">
        <p className="whitespace-pre">digital safety is </p>
      </div>

      <div className="py-1 inline-block font-mono">
        <span className="whitespace-pre inline-block">$ </span>
        <span className="text-ul-bright-blue inline-block">
          <Typewriter
            options={{
              strings: typewriterStrings,
              autoStart: true,
              loop: true,
              cursor: "▉",
            }}
          />
        </span>
      </div>
    </div>
  </div>
)

export default HeroTypewriter
