import React, { ReactElement } from "react"

import SVG2 from "../images/svg/computer_secure_lock.svg"
import SVG0 from "../images/svg/megaphone.svg"
import SVG1 from "../images/svg/tree_diagram.svg"

const ColumnGraphic = (): ReactElement => (
  <div className="container mx-auto px-4">
    <h2 className="max-w-5xl mx-auto py-6 text-left text-xl md:text-2xl">
      The <span className="font-medium">Digital Safety Research Institute</span>{" "}
      (DSRI) is driven by a mission to create a safer digital ecosystem for
      people. As part of{" "}
      <a
        href="https://ul.org/"
        className="text-ul-bright-blue font-medium hover:underline"
      >
        UL Research Institutes
      </a>
      , we are focused on advancing safety as a{" "}
      <span className="font-medium">discipline</span> and as a{" "}
      <span className="font-medium">science</span>.
    </h2>

    <h2 className="max-w-5xl mx-auto py-6 text-left text-xl md:text-2xl">
      Our applied research is focused in three topic areas:
    </h2>

    <div
      className="py-6 mt-6 max-w-7xl mx-auto
                        flex flex-col md:grid md:grid-rows-2 md:grid-flow-col
                        gap-6 md:gap-4"
    >
      <div className="m-auto">
        <SVG0 />
      </div>

      <div>
        <span>
          DSRI aims to <span className="font-bold">empower</span> people. DSRI
          is aggregating existing information and tools to help inform people
          about digital threats and mitigations so consumers can better protect
          themselves and others.
        </span>
      </div>

      <div className="m-auto">
        <SVG1 />
      </div>

      <div>
        <span>
          DSRI is creating tools that enable independent, dynamic{" "}
          <span className="font-bold">digital system evaluations</span> to
          assess and communicate digital system risks. These tools will
          streamline standards and certification processes so that digital
          system assessments will be as dynamic as the evaluated systems.
        </span>
      </div>

      <div className="m-auto">
        <SVG2 />
      </div>

      <div>
        <span>
          Our visionary entrepreneurial researchers are striving to partner with
          digital system developers to develop{" "}
          <span className="font-bold">
            digital products that are safer by design
          </span>
          . We create systems enabling respect for privacy and intellectual
          property while still allowing for increasingly capable digital
          systems.
        </span>
      </div>
    </div>
  </div>
)

export default ColumnGraphic
