import React from "react"

import Button from "./Button"

type HeroSectionProps = {
  className?: string
  text_title: string
  text_content: string
  image: string
  cta?: {
    text: string
    url: string
  }
  reverse?: boolean
}

const HeroSection = ({
  className,
  text_title,
  text_content,
  image,
  cta,
  reverse,
}: HeroSectionProps) => {
  const textDiv = (
    <div className="basis-1/2">
      <div className="max-w-fit mx-auto px-2">
        <h2 className="text-2xl">{text_title}</h2>
        <p className="container pt-4">{text_content}</p>
        {cta && (
          <div className="pt-4">
            <Button text={cta.text} url={cta.url} />
          </div>
        )}
      </div>
    </div>
  )
  const visualContentDiv = (
    <div className="basis-1/2 mx-4 max-w-fit place-content-center">
      <div className="mx-auto">
        <img src={image} />
      </div>
    </div>
  )

  const contents = reverse ? (
    <>
      {" "}
      {visualContentDiv} {textDiv}{" "}
    </>
  ) : (
    <>
      {" "}
      {textDiv} {visualContentDiv}{" "}
    </>
  )

  return (
    <div className={className}>
      <div className="container flex flex-col md:flex-row items-center mx-auto py-10 gap-6">
        {contents}
      </div>
    </div>
  )
}

export default HeroSection
