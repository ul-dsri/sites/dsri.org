import { graphql, navigate } from "gatsby"
import React, { useEffect } from "react"

import { SafetyAlertsPageDocument } from "../../../types.generated"
import Layout from "../../components/Layout"

const isBrowser = () => typeof window !== "undefined"

const SafetyAlertsPage: React.FC<SafetyAlertsPageDocument> = ({ data }) => {
  const alerts = data?.prismicSafetyAlertsPage?.data?.alerts || []

  // Check if the current path is exactly "/safety-alerts/"
  useEffect(() => {
    if (
      isBrowser() &&
      window.location.pathname === "/safety-alerts/" &&
      alerts.length > 0
    ) {
      const latestAlertUid = alerts[0].alert.uid
      navigate(`/safety-alerts/${latestAlertUid}/`, { replace: true })
    }
  }, [alerts])

  // If there are no alerts, display a message
  if (alerts.length === 0) {
    return (
      <Layout>
        <div className="container mx-auto flex items-center flex-col py-4 gap-4">
          <p>There are currently no active Safety Alerts.</p>
          <div>
            Go{" "}
            <a className="prose underline" href="/">
              Home
            </a>
          </div>
        </div>
      </Layout>
    )
  }

  return null // Since we redirect, no need to render anything
}

export const query = graphql`
  {
    prismicSafetyAlertsPage {
      data {
        alerts {
          alert {
            uid
          }
        }
      }
    }
  }
`

export default SafetyAlertsPage
