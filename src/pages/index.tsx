import { PrismicRichText, SliceZone } from "@prismicio/react"
import { graphql, PageProps } from "gatsby"
import { GatsbyImage, IGatsbyImageData } from "gatsby-plugin-image"
import { withPrismicPreview } from "gatsby-plugin-prismic-previews"
import React from "react"

import type {
  HomePageDocumentDataBodyHeroSectionSlice,
  HomePageDocumentDataBodyIconsSectionSlice,
} from "../../types.generated"
import HeroBanner from "../components/HeroBanner"
import Layout from "../components/Layout"
import { SEO } from "../components/SEO"
import linkResolver from "../utils/LinkResolver"

export const Head = () => <SEO />

type HeroSectionSliceProps = {
  slice: HomePageDocumentDataBodyHeroSectionSlice
  slice_type: string
}

type IconsSliceProps = {
  slice: HomePageDocumentDataBodyIconsSectionSlice
  slice_type: string
}

// TODO: Unify this with a component/reduce redundancy of components.
const HeroSectionSlice = ({ slice }: HeroSectionSliceProps) => (
  <div className="group py-6 md:py-12 gap-6 odd:bg-[#F0F8FC] ">
    <div className="container flex flex-col md:flex-row items-center gap-6 mx-auto max-w-6xl">
      <div className="basis-1/2 container md:px-6 order-first md:group-even:order-first md:group-odd:order-last">
        <div className="mx-auto px-4 md:px-0">
          <h2 className="text-2xl md:text-3xl">{slice.primary.title.text}</h2>
          <div className="container pt-4 prose text-black">
            <PrismicRichText field={slice.primary.text.richText} />
          </div>
        </div>
      </div>
      <div className="flex basis-1/2 mx-4 place-content-center">
        <GatsbyImage
          image={slice.primary.image.gatsbyImageData}
          alt={slice.primary.image.alt || ""}
          className="aspect-[5/2] md:aspect-[5/3]"
        />
      </div>
    </div>
  </div>
)

const IconsSectionSlice = ({ slice }: IconsSliceProps) => (
  <div className="group py-6 px-6 md:py-12 md:px-0 gap-6 odd:bg-[#F0F8FC]">
    <div className="container flex flex-col md:flex-row items-stretch gap-10 mx-auto max-w-6xl">
      {slice.items.map(
        (
          item: {
            icon: { gatsbyImageData: IGatsbyImageData; alt: any }
            copy: { richText: any }
          },
          index: React.Key | null | undefined
        ) => (
          <div
            key={index}
            className="flex flex-col items-center justify-center flex-1"
          >
            <div className="w-full aspect-w-1 aspect-h-1 flex items-center justify-center">
              <GatsbyImage
                image={item.icon.gatsbyImageData}
                alt={item.icon.alt || ""}
                className="object-contain"
              />
            </div>
            <div className="container pt-4 prose text-black flex-1 flex items-center justify-center">
              <PrismicRichText field={item.copy.richText} />
            </div>
          </div>
        )
      )}
    </div>
  </div>
)

const IndexPage = ({ data }: PageProps<Queries.HomePageQuery>) => {
  return (
    <Layout>
      {data.prismicHomePage?.data.hero_banner_image?.gatsbyImageData && (
        <HeroBanner
          text={data.prismicHomePage?.data.hero_banner_title.text}
          subtitle={data.prismicHomePage?.data.hero_banner_text.text}
          bannerImage={data.prismicHomePage?.data.hero_banner_image}
        />
      )}

      <div className="container flex flex-col mx-auto my-5 md:my-10 max-w-5xl">
        <p className="text-xl md:text-2xl py-2 px-4 md:leading-relaxed">
          As part of UL Research Institutes, DSRI researches methods to increase
          public safety in the digital ecosystem.
        </p>
        <p className="text-xl md:text-2xl py-2 px-4 md:leading-relaxed">
          Our research is focused in three topic areas:{" "}
        </p>
      </div>

      <SliceZone
        slices={data.prismicHomePage?.data.body}
        components={{
          hero_section: HeroSectionSlice,
          icons_section: IconsSectionSlice,
        }}
      />
    </Layout>
  )
}

export default withPrismicPreview(IndexPage, {
  repositoryName: process.env.GATSBY_PRISMIC_REPO_NAME,
  linkResolver: linkResolver,
})

export const HomePageQuery = graphql`
  query HomePage {
    prismicHomePage(uid: { eq: "homepage" }) {
      id
      _previewable
      data {
        hero_banner_image {
          alt
          gatsbyImageData
          url
        }
        hero_banner_text {
          text
        }
        hero_banner_title {
          text
        }
        body {
          ... on PrismicHomePageDataBodyHeroSection {
            id
            slice_type
            slice_label
            primary {
              cta_link {
                url
              }
              image {
                alt
                gatsbyImageData
                url
              }
              text {
                richText
                text
              }
              title {
                richText
                text
              }
            }
          }
          ... on PrismicHomePageDataBodyRichTextContent {
            id
            slice_type
            slice_label
            primary {
              content {
                richText
                text
              }
            }
          }
          ... on PrismicHomePageDataBodyIconsSection {
            id
            slice_type
            slice_label
            items {
              icon {
                alt
                gatsbyImageData
                url
              }
              copy {
                richText
                text
              }
            }
          }
        }
      }
    }
  }
`
