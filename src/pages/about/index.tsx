import { PrismicRichText, SliceZone } from "@prismicio/react"
import { graphql, PageProps } from "gatsby"
import React from "react"

import AdvisoryBoardSection from "../../components/AdvisoryBoardSection"
import Page from "../../components/Page"
import ProseBlock from "../../components/ProseBlock"
import { SEO } from "../../components/SEO"
import TeamSection from "../../components/TeamSection"

export const Head = (props: PageProps) => (
  <SEO
    title="About"
    description="DSRI, part of the UL Research Institutes, advances digital safety and innovation. Discover our role in a century-long legacy of enhancing public safety."
    pathname={props.location.pathname}
  />
)

const AboutPage = ({ data }: PageProps<Queries.AboutPageQuery>) => (
  <Page title="About Us">
    <ProseBlock>
      <PrismicRichText field={data.prismicAboutPage?.data.main_text.richText} />
    </ProseBlock>
    <SliceZone
      slices={data.prismicTeamPage?.data.body}
      components={{
        team_section: TeamSection,
      }}
    />
    <SliceZone
      slices={data.prismicTechnicalLeadsPage?.data.body}
      components={{
        team_section: TeamSection,
      }}
    />
    <SliceZone
      slices={data.prismicAdvisoryBoard?.data.body}
      components={{
        advisory_board_member: AdvisoryBoardSection,
      }}
    />
  </Page>
)

export default AboutPage

export const AboutPageQuery = graphql`
  query AboutPage {
    prismicAboutPage(uid: { eq: "about" }) {
      id
      data {
        main_text {
          richText
        }
      }
    }
    prismicTeamPage(uid: { eq: "team" }) {
      data {
        body {
          ... on PrismicTeamPageDataBodyTeamSection {
            id
            slice_type
            slice_label
            primary {
              title {
                text
              }
            }
            items {
              team_profile {
                document {
                  ... on PrismicTeamProfile {
                    id
                    data {
                      bio {
                        richText
                        text
                      }
                      name
                      profile_picture {
                        alt
                        gatsbyImageData
                        url
                      }
                      title_info
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    prismicTechnicalLeadsPage(uid: { eq: "technical_leads" }) {
      data {
        body {
          ... on PrismicTechnicalLeadsPageDataBodyTeamSection {
            id
            slice_type
            slice_label
            primary {
              title {
                text
              }
            }
            items {
              team_profile {
                document {
                  ... on PrismicTeamProfile {
                    id
                    data {
                      bio {
                        richText
                        text
                      }
                      name
                      profile_picture {
                        alt
                        gatsbyImageData
                        url
                      }
                      title_info
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    prismicAdvisoryBoard(uid: { eq: "advisory_board_page" }) {
      data {
        body {
          ... on PrismicAdvisoryBoardDataBodyAdvisoryBoardMember {
            id
            slice_type
            slice_label
            primary {
              intro {
                richText
              }
            }
            items {
              advisory_board_member {
                id
                document {
                  ... on PrismicAdvisoryBoardMember {
                    data {
                      name
                      name_link {
                        url
                        target
                      }
                      bio {
                        richText
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
