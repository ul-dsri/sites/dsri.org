import React, { useEffect, useState } from "react"

import {
  BlogPostDocument,
  BlogPostDocumentDataAuthorsItem,
} from "../../../types.generated"
import BlogPostContent from "../../components/BlogPostContent"
import Layout from "../../components/Layout"

const fetchAuthorDataById = async (id: any, token: any) => {
  const apiUrl = `https://${process.env.GATSBY_PRISMIC_REPO_NAME}.cdn.prismic.io/api/v2/documents/search?ref=${token}&q=${encodeURIComponent(`[[at(document.id, "${id}")]]`)}`
  const response = await fetch(apiUrl)
  const json = await response.json()
  return json.results[0] || null
}

const transformDocumentData = async (doc: BlogPostDocument, token: any) => {
  // Fetch and transform authors with additional data
  const authors = await Promise.all(
    doc?.data?.authors.map(async (author: BlogPostDocumentDataAuthorsItem) => {
      const authorData = await fetchAuthorDataById(
        author.team_profile.id,
        token
      )
      return {
        team_profile: {
          document: {
            data: {
              name: authorData?.data?.name,
              title_info: authorData?.data?.title_info,
              profile_picture: {
                alt: authorData?.data?.profile_picture?.alt,
                gatsbyImageData: authorData?.data?.profile_picture?.url
                  ? {
                      images: {
                        sources: [
                          { srcSet: authorData.data.profile_picture.url },
                        ],
                      },
                    }
                  : null,
              },
            },
          },
        },
      }
    })
  )

  return {
    cover_image: {
      alt: doc?.data?.cover_image?.alt,
      gatsbyImageData: doc?.data?.cover_image?.url
        ? {
            images: {
              sources: [
                {
                  srcSet: doc.data.cover_image.url,
                  sizes: "(min-width: 2500px) 2500px, 100vw",
                  type: "image/webp",
                },
              ],
              fallback: {
                srcSet: doc.data.cover_image.url,
                sizes: "(min-width: 2500px) 2500px, 100vw",
                type: "image/webp",
              },
            },
            backgroundColor: "#516ca0",
            height: 1506,
            layout: "constrained",
            width: 2500,
          }
        : null,
    },
    authors,
    content: {
      richText: doc?.data?.content,
    },
    date: doc?.data?.date,
    external_link: {
      url: doc?.data?.external_link?.url,
    },
    subtitle: {
      text: doc?.data?.subtitle[0]?.text,
    },
    tags: doc?.data?.tags,
    title: {
      text: doc?.data?.title[0]?.text,
    },
    preview_text: {
      text: doc?.data?.preview_text[0]?.text,
    },
  }
}

const NewBlogPostPreviewPage = ({ location }: any) => {
  const [document, setDocument] = useState({})

  useEffect(() => {
    const fetchPreview = async () => {
      const token = new URLSearchParams(location.search).get("token")
      if (!token) {
        console.error("No preview token found in the URL.")
        return
      }

      try {
        const apiUrl = `https://${process.env.GATSBY_PRISMIC_REPO_NAME}.cdn.prismic.io/api/v2/documents/search?ref=${token}`
        const response = await fetch(apiUrl)

        if (!response.ok) {
          console.error("Failed to fetch document:", response.statusText)
          return
        }

        const json = await response.json()

        if (json.results && json.results.length > 0) {
          const doc = json.results[0]

          // Transform the document to match the expected structure
          const transformedData = await transformDocumentData(doc, token)
          setDocument(transformedData)
        } else {
          console.error("No document found for the provided preview token.")
        }
      } catch (error) {
        console.error("Error fetching preview document:", error)
      }
    }

    fetchPreview()
  }, [location])

  if (!document) {
    return <p>Loading preview…</p>
  }

  return (
    <Layout>
      <BlogPostContent blogPost={{ data: document }} />
    </Layout>
  )
}

export default NewBlogPostPreviewPage
