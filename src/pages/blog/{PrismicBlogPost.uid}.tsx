import { graphql, PageProps } from "gatsby"
import { withPrismicPreview } from "gatsby-plugin-prismic-previews"
import React from "react"

import BlogPostContent from "../../components/BlogPostContent"
import Layout from "../../components/Layout"
import { SEO } from "../../components/SEO"
import linkResolver from "../../utils/LinkResolver"

export const Head = ({ data }: PageProps<Queries.BlogPostQuery>) => {
  const metaImage =
    data.prismicBlogPost?.data?.cover_image?.gatsbyImageData?.images?.fallback
      ?.src

  return (
    <SEO
      title={data.prismicBlogPost?.data.title.text || undefined}
      description={data.prismicBlogPost?.data.preview_text.text || undefined}
      metaImage={metaImage || undefined}
      pathname={`/blog/${data.prismicBlogPost?.uid}`}
    />
  )
}

const BlogPostPage = ({ data }: PageProps<Queries.BlogPostQuery>) => {
  const blogPost = data.prismicBlogPost

  return (
    <Layout>
      <BlogPostContent blogPost={blogPost} />
    </Layout>
  )
}

export default withPrismicPreview(BlogPostPage, {
  repositoryName: process.env.GATSBY_PRISMIC_REPO_NAME,
  linkResolver: linkResolver,
})

export const BlogPostPageQuery = graphql`
  query BlogPost($uid: String) {
    prismicBlogPost(uid: { eq: $uid }) {
      uid
      tags
      _previewable
      data {
        cover_image {
          alt
          gatsbyImageData
        }
        authors {
          team_profile {
            document {
              ... on PrismicTeamProfile {
                __typename
                data {
                  name
                  title_info
                  profile_picture {
                    alt
                    gatsbyImageData
                  }
                }
              }
            }
          }
        }
        content {
          richText
        }
        date
        external_link {
          url
        }
        subtitle {
          text
        }
        tags {
          tag
        }
        title {
          text
        }
        preview_text {
          text
        }
      }
    }
  }
`
