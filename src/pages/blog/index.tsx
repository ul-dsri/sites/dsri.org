import { graphql, PageProps } from "gatsby"
import { withPrismicPreview } from "gatsby-plugin-prismic-previews"
import React from "react"

import BlogPostPreviewItem from "../../components/BlogPostPreviewItem"
import Page from "../../components/Page"
import ProseBlock from "../../components/ProseBlock"
import { SEO } from "../../components/SEO"
import linkResolver from "../../utils/LinkResolver"
import { dateToUTC } from "../../utils/prismic"

export const Head = (props: PageProps) => (
  <SEO
    title="Blog"
    description="News, updates, and editorials from our DSRI expert researchers."
    pathname={props.location.pathname}
  />
)

const BlogPage = ({ data }: PageProps<Queries.BlogPageQuery>) => (
  <Page title="Blog">
    <ProseBlock>
      <p>News, updates, and editorials from our DSRI expert researchers.</p>
    </ProseBlock>
    <div className="grid place-items-stretch sm:grid-cols-2 lg:grid-cols-3 gap-6 mb-4">
      {data.allPrismicBlogPost.nodes.map((post) => {
        let date = dateToUTC(post.data.date)

        return (
          <BlogPostPreviewItem
            key={post.uid}
            destination={post.data.external_link?.url || `/blog/${post.uid}`}
            isExternal={post.data.external_link?.url || false}
            title={post.data.title?.text}
            date={date}
            preview={post.data.preview_text?.text}
            thumbnailImage={post.data.cover_image}
            tags={post.data.tags?.map((tag_obj) => tag_obj.tag)}
          />
        )
      })}
    </div>
  </Page>
)

export default withPrismicPreview(BlogPage, {
  repositoryName: process.env.GATSBY_PRISMIC_REPO_NAME,
  linkResolver: linkResolver,
})

export const BlogPageQuery = graphql`
  query BlogPage {
    allPrismicBlogPost(sort: { data: { date: DESC } }) {
      nodes {
        uid
        _previewable
        data {
          cover_image {
            alt
            gatsbyImageData
          }
          date
          external_link {
            url
          }
          preview_text {
            text
          }
          tags {
            tag
          }
          title {
            text
          }
        }
      }
    }
  }
`
