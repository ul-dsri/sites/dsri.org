import { withPrismicUnpublishedPreview } from "gatsby-plugin-prismic-previews"
import React from "react"

import Layout from "../components/Layout"

const NotFoundPage = () => (
  <Layout>
    <div className="container mx-auto max-w-4xl pt-6 pb-6 px-4 text-center">
      <h1 className="text-3xl">404</h1>
      <br />
      <p>Oops.</p>
    </div>
  </Layout>
)

export default withPrismicUnpublishedPreview(NotFoundPage)
