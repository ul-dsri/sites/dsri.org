/** @type {import('tailwindcss').Config} */

// Required to extend default theme.
const defaultTheme = require("tailwindcss/defaultTheme")

module.exports = {
  content: [
    `./src/pages/**/*.{js,jsx,ts,tsx}`,
    `./src/components/**/*.{js,jsx,ts,tsx}`,
    `./src/templates/**/*.{js,jsx,ts,tsx}`,
  ],
  theme: {
    extend: {
      screens: {
        xs: "480px",
      },
      colors: {
        "ul-aqua": "#577E9E",
        "ul-bright-blue": "#0A32FF",
        "ul-coral": "#FF6255",
        "ul-dark-blue": "#000095",
        "ul-fog": "#577E9E",
        "ul-gray-1": "#d8d9da",
        "ul-gray-2": "#bcbec0",
        "ul-gray-3": "#939698",
        "ul-gray-4": "#58595b",
        "ul-gray-background": "#ebebeb",
        "ul-light-blue": "#BCE4F7",
        "ul-midnight-blue": "#122C49",
        "ul-pebble": "#E5DDCA",
      },
      fontFamily: {
        sans: ["Poppins", ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
}
