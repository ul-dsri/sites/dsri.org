exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const result = await graphql(`
    {
      prismicSafetyAlertsPage {
        data {
          alerts {
            alert {
              uid
            }
          }
        }
      }
    }
  `)

  const safetyAlertTemplate = require.resolve(
    "./src/templates/safety-alert.tsx"
  )
  const latestAlert =
    result.data.prismicSafetyAlertsPage?.data?.alerts[0]?.alert

  // Create individual pages for each safety alert
  result.data.prismicSafetyAlertsPage?.data?.alerts.forEach((node) => {
    if (node.alert && node.alert.uid) {
      createPage({
        path: `/safety-alerts/${node.alert.uid}/`,
        component: safetyAlertTemplate,
        context: {
          uid: node.alert.uid,
        },
      })
    }
  })

  // Create the index page for /safety-alerts/
  createPage({
    path: `/safety-alerts/`,
    component: require.resolve("./src/pages/safetyAlerts/index.tsx"),
  })
}
